import typer
import pandas as pd
import sendpostback as sp
from tabulate import tabulate

def main(archivo:str):
    data_import = pd.read_csv(archivo,        
                          skiprows = 1,
                          names = ['clickid', 'comission', 'currency', 'network_code', 'account_code', 'referral', 'status', 'transactionId'])
    
    
    print(tabulate(data_import.head(), headers='keys', tablefmt='psql' ))
    
    total_comission = data_import['comission'].sum()
    print(f'Total comission is : {total_comission}')

    lista = []
    for index, row in data_import.iterrows():
        lista.append(sp.process_df_to_pburl(row['clickid'],str(row['comission']), row['currency'], row['network_code'], row['account_code'], row['referral'], row['status'], str(row['transactionId'])))

    for elem in lista:
        print(elem)
        #sp.send_cid_to_braintag(elem)
    typer.echo(f"number of records processed: {len(data_import.index)}")


if __name__ == "__main__":
    typer.run(main)